#define DHT_PIN 15
#define LED_PIN 2
#define NOISE_PIN 4
#define FAN_PIN_IN1 26
#define FAN_PIN_IN2 25
#define FAN_PIN_IN3 33
#define FAN_PIN_IN4 32

#define humidityTopic "sensor/humidity"
#define temperatureTopic "sensor/temperature"
#define angleTopic "sensor/angle"
#define detectNoiseTopic "sensor/detectNoise"
#define reqFanOnTopic "sensor/reqFanOn"
#define reqFanOffTopic "sensor/reqFanOff"

#define prefixTopic "it4735/"