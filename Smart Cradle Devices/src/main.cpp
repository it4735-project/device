#include <stdafx.h>

WiFiClient espClient;
PubSubClient client(espClient);
DHT dht(DHT_PIN, DHT22);

char buffer[256];
std::string messageRcv;
long defaultInterval = 10000;
unsigned long lastTime = 0;

void setup()
{
  // put your setup code here, to run once:
  Serial.begin(9600);
  pinMode(LED_PIN, OUTPUT);
  pinMode(NOISE_PIN, INPUT);
  Wire.begin();
  Wire.beginTransmission(0x68);
  Wire.write(0x6B);
  Wire.write(0);
  Wire.endTransmission(true);

  connectWifi();
  dht.begin();
  connectToMqttBroker();

  client.subscribe(MQTT_TOPIC_CONTROL);
}

void loop()
{
  client.loop();

  if (millis() - lastTime > defaultInterval)
  {
    lastTime = millis();
    getDeviceState();
  }
}

void callback(char *topic, byte *payload, unsigned int length)
{
  Serial.print("Message arrived in topic: ");
  Serial.println(topic);
  messageRcv = "";
  for (int i = 0; i < length; i++)
  {
    messageRcv += (char)payload[i];
  }
  Serial.println(messageRcv.c_str());

  // change message to json
  StaticJsonDocument<256> doc;
  deserializeJson(doc, messageRcv.c_str());
  int type = doc["type"];

  // type = 1 => turn on/off light
  if (type == 1)
  {
    int light = doc["light"];
    int value = doc["value"];

    digitalWrite(LED_PIN, value);
  }
  else if (type == 2)
  {
  }
  else if (type == 3)
  {
    // type = 3 => change interval
    defaultInterval = doc["value"];
  }
  else if (type == 4)
  {
    getDeviceState();
  }
}

void connectWifi()
{
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.println("Connecting to WiFi..");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(1000);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("Connected to the WiFi network");
}

void connectToMqttBroker()
{
  client.setServer(MQTT_BROKER, MQTT_PORT);
  client.setCallback(callback);
  while (!client.connected())
  {
    String client_id = "ESP32-client-";
    client_id += String(WiFi.macAddress());
    Serial.printf("The client %s connects to the public mqtt broker\n", client_id.c_str());
    if (client.connect(client_id.c_str(), MQTT_USERNAME, MQTT_PASSWORD))
    {
      Serial.println("Public emqx mqtt broker connected");
    }
    else
    {
      Serial.print("failed with state ");
      Serial.print(client.state());
      delay(2000);
    }
  }
}

void getDeviceState()
{
  StaticJsonDocument<256> doc;
  DHTData dhtData = measure_temp_humid(dht);
  doc["temperature"] = dhtData.temperature;
  doc["humidity"] = dhtData.humidity;
  doc["cradleSku"] = "CRA51";
  doc["light"] = digitalRead(LED_PIN);
  doc["noise"] = digitalRead(NOISE_PIN);
  doc["cycle"] = defaultInterval;
  doc["lean"] = getAngle();
  doc["fan"] = 0;
  std::string msg;
  serializeJson(doc, msg);
  client.publish(MQTT_TOPIC_SEND_DATA, msg.c_str());
  msg = "";
}