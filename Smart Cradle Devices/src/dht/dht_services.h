#include <DHT.h>
#include <configs/constants.h>

float temperature, humidity;

struct DHTData
{
    float temperature;
    float humidity;
};

struct DHTData measure_temp_humid(DHT dht)
{
    temperature = dht.readTemperature();
    humidity = dht.readHumidity();

    DHTData data = {temperature, humidity};
    Serial.print("Temperature: ");
    Serial.print(temperature);
    Serial.print(" *C, Humidity: ");
    Serial.print(humidity);
    Serial.println(" %");
    return data;
}
