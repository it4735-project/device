// Include some libraries necessary for the project and define some functions
#include <Arduino.h>
#include <Adafruit_Sensor.h>
#include <DHT_U.h>
#include <WiFi.h>
#include <Wire.h>
#include <ArduinoJson.h>
#include <PubSubClient.h>
#include <DHT.h>
#include <env.h>
#include <configs/constants.h>
#include <dht/dht_services.h>
#include <angle/angle_service.h>

void callback(char *topic, byte *payload, unsigned int length);
void connectWifi();
void connectToMqttBroker();
void getDeviceState();